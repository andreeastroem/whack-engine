#include "stdafx.h"

#include "NavigationPath.h"

#include "NavigationGrid.h"

namespace wh
{

	NavigationGrid::NavigationGrid()
	{
		m_nodeVertices.setPrimitiveType(sf::Points);
		m_pathFindingVertices.setPrimitiveType(sf::LinesStrip);
	}

	NavigationGrid::~NavigationGrid()
	{
		Clean();
	}

	bool NavigationGrid::Construct(sf::Vector2f pStartPosition, sf::Vector2i pSize, sf::Vector2f pNodeInterval)
	{
		m_startposition = pStartPosition;
		m_size = pSize;
		m_nodeInterval = pNodeInterval;

		Clean();
		
		std::vector<NavigationNode*> tempColumn;

		for (unsigned int column = 0; column < m_size.x; column++)
		{
			for (unsigned int row = 0; row < m_size.y; row++)
			{
				tempColumn.push_back(new NavigationNode(sf::Vector2f(column * pNodeInterval.x + pStartPosition.x, row * pNodeInterval.y + pStartPosition.y), sf::Vector2i(column, row)));

				//debugging
				sf::Vertex vertex(sf::Vector2f(column * pNodeInterval.x + pStartPosition.x, row * pNodeInterval.y + pStartPosition.y), m_nodeColour);
				m_nodeVertices.append(vertex);
			}

			m_grid.push_back(tempColumn);

			tempColumn.clear();
		}

		return true;
	}

	void NavigationGrid::Clean()
	{
		for each (std::vector<NavigationNode*> row in m_grid)
		{
			for each (NavigationNode* var in row)
			{
				if (var)
				{
					var->Leave();
					delete var;
					var = nullptr;
				}
			}
		}

		m_nodeVertices.clear();
		m_pathFindingVertices.clear();
	}

	NavigationPath* NavigationGrid::FindPath(sf::Vector2f pStart, sf::Vector2f pTarget)
	{
		sf::FloatRect grid = sf::FloatRect(m_startposition, sf::Vector2f(m_size.x * m_nodeInterval.x, m_size.y * m_nodeInterval.y));

		if (grid.contains(pStart) && grid.contains(pTarget))
		{
			int x = static_cast<int>((pStart.x - m_startposition.x) / m_nodeInterval.x);
			int y = static_cast<int>((pStart.y - m_startposition.y) / m_nodeInterval.y);
			NavigationNode* nodeStart = m_grid[x][y];

			printf("Start x: %i y: %i\n", x, y);

			x = static_cast<int>((pTarget.x - m_startposition.x) / m_nodeInterval.x);
			y = static_cast<int>((pTarget.y - m_startposition.y) / m_nodeInterval.y);
			NavigationNode* nodeTarget = m_grid[x][y];;

			printf("Goal x: %i y: %i\n", x, y);

			/************************************************************************/
			/* How the grid is assembled
			 * x1 x2 x3 x4 x5 x6 etc
			 * y1 y1 y1 y1 y1 y1
			 * y2 y2 y2 y2 y2 y2
			 * y3 y3 y3 y3 y3 y3
			 * y4 y4 y4 y4 y4 y4
			 * y5 y5 y5 y5 y5 y5
			 * y6 y6 y6 y6 y6 y6
			 * etc
			/************************************************************************/

			weightedNavigationNode* start = new weightedNavigationNode(nodeStart, 0, 0, 0);
			std::vector<weightedNavigationNode*> openList;
			std::vector<weightedNavigationNode*> closedList;

			

			openList.push_back(start);

			weightedNavigationNode* current = openList[0];
			int currentI;

			//Lambdas
			auto GetLowestF = [&]() -> int
			{
				weightedNavigationNode* lowestF = openList[0];
				unsigned int lowestIndex = 0, i = 0;
				for each (weightedNavigationNode* var in openList)
				{
					if (lowestF->m_f > openList[i]->m_f)
					{
						lowestF = openList[i];
						lowestIndex = i;
					}
					i++;
				}
				return lowestIndex;
			};
			auto IsInClosedList = [&](NavigationNode* pNode) -> bool
			{
				for each (weightedNavigationNode* var in closedList)
				{
					if (var->m_node == pNode)
						return true;
				}
				return false;
			};
			auto IsInOpenList = [&](NavigationNode* pNode, int& pOut) -> bool
			{
				int i = 0;
				for each (weightedNavigationNode* var in openList)
				{
					if (var->m_node == pNode)
					{
						pOut = i;
						return true;
					}
					i++;
				}
				return false;
			};
			auto Backtrack = [&](weightedNavigationNode* pEnd, NavigationNode* pStart)-> NavigationPath*
			{
				std::vector<NavigationNode*> pathNodes;
				weightedNavigationNode* current = pEnd;

				m_pathFindingVertices.clear();

				while (current->m_node != pStart)
				{
					pathNodes.push_back(current->m_node);

					sf::Vertex vertex(current->m_node->getPosition(), m_pathFindingColour);
					m_pathFindingVertices.append(vertex);

					current = current->m_parent;
				}
				m_pathFindingVertices.append(sf::Vertex(pStart->getPosition(), m_pathFindingColour));

				for each (weightedNavigationNode* var in openList)
				{
					delete var;
					var = nullptr;
				}
				for each (weightedNavigationNode* var in closedList)
				{
					delete var;
					var = nullptr;
				}

				closedList.clear();
				openList.clear();

				return new NavigationPath(pathNodes);
			};
			auto CheckNeighbour = [&](int x, int y)
			{
				sf::Vector2f parentPos = current->m_node->getPosition();
				
				if (NodeWithinBounds(current->m_node->getGridIndex().x + x, current->m_node->getGridIndex().y + y))
				{
					NavigationNode* n = m_grid[current->m_node->getGridIndex().x + x][current->m_node->getGridIndex().y + y];
					sf::Vector2f neighbourPos = n->getPosition();

					if (n->isWalkable() && !IsInClosedList(n))
					{
						int openIndex = 0;

						float g, f;
						int h;

						sf::Vector2f deltaPos = neighbourPos - parentPos;
						g = current->m_g + 1 + n->GetTerrainToughness();

						if (deltaPos.x != 0 && deltaPos.y != 0)
							g += 0.414f;

						h = abs(pTarget.x - neighbourPos.x) + abs(pTarget.y - neighbourPos.y);
						f = g + h;

						if (IsInOpenList(n, openIndex))
						{
							weightedNavigationNode* neighbourNode = openList[openIndex];
							if (neighbourNode->m_f < f)
								*neighbourNode = weightedNavigationNode(n, f, g, h, current);
						}
						else
						{
							weightedNavigationNode* neighbourNode = new weightedNavigationNode(n, f, g, h, current);
							openList.push_back(neighbourNode);
						}
					}
				}
			};

			while (openList.size() > 0)
			{
				currentI = GetLowestF();
				current = openList[currentI];

				//THE NEXT LINE COULD CONTAIN MEMORY LEAKS
				openList.erase(openList.begin() + currentI);

				closedList.push_back(current);

				if (current->m_node == nodeTarget)
					return Backtrack(current, nodeStart);

				CheckNeighbour(0, -1);
				CheckNeighbour(1, -1);

				CheckNeighbour(-1, 0);
				CheckNeighbour(1, 0);

				CheckNeighbour(0, 1);
				CheckNeighbour(1, 1);
			}

			for each (weightedNavigationNode* var in openList)
			{
				delete var;
				var = nullptr;
			}
			for each (weightedNavigationNode* var in closedList)
			{
				delete var;
				var = nullptr;
			}

			closedList.clear();
			openList.clear();

			return nullptr;
		}
		else
		{
			printf("target destination does not exist in the grid\nEntered position: X: %0.2f, Y: %0.2f\n", pTarget.x, pTarget.y);
			return nullptr;
		}
	}

	void NavigationGrid::SetDebugColour(sf::Color pNodeColour, sf::Color pPathFindingColour)
	{
		m_nodeColour = pNodeColour;
		m_pathFindingColour = pPathFindingColour;
	}

	void NavigationGrid::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.texture = NULL;

		target.draw(m_nodeVertices, states);
		target.draw(m_pathFindingVertices, states);
	}

	bool NavigationGrid::NodeWithinBounds(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < m_size.x && y < m_size.y)
			return true;
		return false;
	}

	NavigationNode* NavigationGrid::getNodeAtPosition(sf::Vector2f pPosition)
	{
		int x = static_cast<int>((pPosition.x - m_startposition.x) / m_nodeInterval.x + 0.5f);
		int y = static_cast<int>((pPosition.y - m_startposition.y) / m_nodeInterval.y + 0.5f);
		return m_grid[x][y];
	}

	void NavigationGrid::AlterNode(sf::Vector2f pPosition, Object* pOccupant, bool pWalkableState)
	{
		getNodeAtPosition(pPosition)->Occupy(pOccupant, pWalkableState);
		getDebugNodeAtPosition(pPosition).color = sf::Color::Red;
	}

	sf::Vertex& NavigationGrid::getDebugNodeAtPosition(sf::Vector2f pPosition)
	{
		int x = static_cast<int>((pPosition.x - m_startposition.x) / m_nodeInterval.x + 0.5f);
		int y = static_cast<int>((pPosition.y - m_startposition.y) / m_nodeInterval.y + 0.5f);
		return m_nodeVertices[x * m_size.y + y];
	}

	NavigationGrid::weightedNavigationNode::weightedNavigationNode(NavigationNode* pNode, float pF, float pG, int pH, weightedNavigationNode* pParent /*= nullptr*/)
	{
		m_parent = pParent;
		m_node = pNode;
		m_f = pF;
		m_g = pG;
		m_h = pH;
	}

	bool NavigationGrid::weightedNavigationNode::operator>(weightedNavigationNode pNode)
	{
		if (m_f > pNode.m_f)
			return true;
		return false;
	}

	bool NavigationGrid::weightedNavigationNode::operator<(weightedNavigationNode pNode)
	{
		if (m_f < pNode.m_f)
			return true;
		return false;
	}

}