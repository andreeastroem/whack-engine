/************************************************************************/
/* Base class for all rendering components
/* Includes functionality of fetching the data required to draw
/* and what type of render component it is
/************************************************************************/

#pragma once

#include "Component.h"
#include "SFML/Graphics.hpp"

namespace wh
{
	
	enum RenderType
	{
		POINT,
		LINE,
		TRIANGLE,
		RECTANGLE,
		CIRCLE,
		SPRITE,
		TEXT,
		RENDERTYPESIZE
	};
	class RenderComponent : public Component
	{
	public:
		RenderComponent();
		~RenderComponent();

		//Inherited functions from Component
		virtual bool Initialise(const char* pName = "render") override;
		virtual void Update(float pDeltatime) override;
		virtual void Cleanup() override;

		//Virtual classes
		virtual RenderType GetRenderType();
		virtual sf::Drawable* GetRenderData() = 0;
		virtual void SetColour(sf::Color pColour) = 0;
		virtual sf::Color GetColour() = 0{};
		virtual sf::FloatRect GetBounds() = 0{};

	private:

	public:

	private:

	protected:
		RenderType m_type;
	};
	
}