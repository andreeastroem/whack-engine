#include "stdafx.h"

#include "Object.h"

#include "Hash.h"

namespace wh
{
	Object::Object()
	{

	}

	Object::Object(unsigned int pUniqueID)
	{
		m_UniqueID = pUniqueID;
	}

	Object::~Object()
	{
		Cleanup();
	}

	void Object::Update(float pDeltatime)
	{
		/*for each (Component* var in m_components)
		{
		if (var)
		var->Update(pDeltatime);
		}*/

		auto it = m_components.begin();

		while (it != m_components.end())
		{
			it->second->Update(pDeltatime);
			it++;
		}
	}

	void Object::Cleanup()
	{
		/*for each (Component* var in m_components)
		{
		if (var)
		{
		var->Cleanup();
		delete var;
		var = nullptr;
		}
		}*/

		auto it = m_components.begin();

		while (it != m_components.end())
		{
			if (it->second)
			{
				it->second->Cleanup();
				delete it->second;
				it->second = nullptr;
				it++;
			}
		}

		m_components.clear();
	}

	void Object::Destroy()
	{
		m_isDestroyed = true;
	}

	bool Object::GetDestroyed()
	{
		return m_isDestroyed;
	}

	unsigned int Object::GetUniqueID()
	{
		return m_UniqueID;
	}

	void Object::AttachComponent(Component* pComponent)
	{
		//m_components.push_back(pComponent);

		std::pair<int, Component*> p = std::pair<int, Component*>(pComponent->getHashedID(), pComponent);
		m_components.insert(p);
	}

	bool Object::HasComponent(const char* pComponentName)
	{
		auto it = m_components.find(util::Hash::hash(pComponentName));
		if (it != m_components.end())
			return true;
		return false;
	}

}


