#pragma once

#include "GUIObject.h"
#include "ObjectManager.h"

#include "Factory.h"

namespace wh
{
	namespace system
	{
		class GUIFactory : public Factory
		{
		public:
			GUIFactory(ObjectManager* pObjectManager);
			~GUIFactory();

			void Cleanup();

			GUIObject* CreateGUIObject(const char* pName, Transform pTransform = Transform());
			bool CreateTextField();
			bool CreateButton(float x, float y, float width, float height, GUIObject& pObject, const char* pLabel = "button");
			bool CreateCheckBox();

		private:
			ObjectManager* m_objectManager;
		};
	}
}