#include "stdafx.h"

#include "Object.h"

#include "component.h"

#include "Hash.h"

namespace wh
{

	bool Component::Initialise(const char* pName)
	{
		m_name = pName;

		m_id = util::Hash::hash(m_name);

		return true;
	}

	void Component::Update(float pDeltatime)
	{

	}

	void Component::Cleanup()
	{
		if (m_owner)
			m_owner = nullptr;
		if (m_parent)
			m_parent = nullptr;
	}

	int Component::getHashedID()
	{
		return m_id;
	}

	void Component::SetOwner(Object* pOwner)
	{
		m_owner = pOwner;
	}

	void Component::SetParentComponent(Component* pParent)
	{
		m_parent = pParent;
	}

}