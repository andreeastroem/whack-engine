#include "stdafx.h"

#include "Controller.h"

namespace wh
{

	Controller::Controller()
	{

	}

	Controller::~Controller()
	{

	}

	bool Controller::Initialise(const char* pName)
	{
		__super::Initialise(pName);
		
		return true;
	}

	void Controller::Update(float pDeltatime)
	{

	}

	void Controller::Cleanup()
	{
		__super::Cleanup();
		
	}

}