#pragma once

#include "ScriptComponent.h"

#include "Task.h"

namespace wh
{
	class GUIScriptComponent : public ScriptComponent
	{
	public:
		GUIScriptComponent();
		~GUIScriptComponent();

		virtual void OnClick();
		virtual void OnHover();
		virtual void ExitHover();

		virtual bool Initialise(const char* pName = "script") override;
		virtual void Update(float pDeltatime) override;
		virtual void Cleanup() override;

		virtual void SetOnClickFunction(TaskBase* pTask);

	protected:
		TaskBase* m_task;
	};
}