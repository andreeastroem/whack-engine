//InputSystem.cpp

#include "stdafx.h"

#include "SFML/Graphics.hpp"

#include "EventListener.h"
#include "ServiceLocator.h"
#include "GUI.h"

#include "Engine.h"

//namespaces
using namespace wh::system;

/*
----------------------------------------------
Constructors and deconstructors
----------------------------------------------
*/
EventListener::EventListener()
{
	
}
EventListener::~EventListener()
{

}

/*
----------------------------------------------
public functions
----------------------------------------------
*/
EventListener::ptr EventListener::Create()
{
	return EventListener::ptr(new EventListener());
}
void EventListener::Update()
{
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/

void EventListener::HandleEvent(sf::Event& pEvent)
{
	switch (pEvent.type)
	{
	case sf::Event::MouseButtonPressed:
		if (pEvent.mouseButton.button == sf::Mouse::Left)
		{
			ServiceLocator<GUI>::GetService()->MouseButtonPress(pEvent.mouseButton.button);
		}
		break;
	default:
		break;
	}
}
