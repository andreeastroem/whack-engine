#pragma once

#include "DebuggingTool.h"

class graph : public debuggingTool
{
public:
	graph();
	graph(unsigned int pWidth, unsigned int pHeight);
	~graph();

	virtual bool initialise(float pUpdateIntervall) override;

	virtual void update(float pDeltatime) override;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void SetXName(sf::String pName);
	void SetYName(sf::String pName);
	void SetXMax(float pMax);
	void SetYMax(float pMax);

	void AddValue(float pValue);
	
	void SetDynamic(bool pState);

	void SetColour(sf::Color pColour);

	void SetPosition(float pPosX, float pPosY);

	bool CanUpdate();

private:
	sf::VertexArray m_vertices;
	sf::Texture m_texture;

	sf::Font m_font;

	sf::Text m_XName;
	sf::Text m_YName;

	//Size
	bool m_dynamic;

	unsigned int m_xMax;
	unsigned int m_yMax;

	float m_height;
	float m_width;

	//position
	float m_posX;
	float m_posY;

	//Colours
	sf::Color m_pointColour;
	sf::Color m_backgroundColour;


	float m_updateIntervall;
	float m_currentUpdateDelta;
	bool m_canUpdate;
};