//Engine.cpp

#include "stdafx.h"

#include "Application.h"
//#include "EventListener.h"

#include "Engine.h"

#include "ServiceLocator.h"

#include "typedefs.h"

//tests
#include "GUIObject.h"
#include "Task.h"

using namespace wh;

/*
-----------------------------------------------
Constructors and destructor
-----------------------------------------------
*/
Engine::Engine()
{

}
Engine::~Engine()
{
	Cleanup();
}


/*
----------------------------------------------
public functions
----------------------------------------------
*/
bool Engine::Initialise(sf::RenderWindow& pWindow)
{
	printf("hash id: %i\n", util::Hash::hash("script"));

	m_renderSystem = system::RenderSystem::Create(pWindow);
#ifdef _DEBUG
	system::Log::Message("RenderSystem created.", system::Log::SUCCESSFUL);
#endif
	m_eventListener = system::EventListener::Create();
#ifdef _DEBUG
	system::Log::Message("EventListener created.", system::Log::SUCCESSFUL);
#endif
	m_objectManager = system::ObjectManager::Create();
#ifdef _DEBUG
	system::Log::Message("ObjectManager created.", system::Log::SUCCESSFUL);
#endif
	m_gui = system::GUI::Create(&pWindow);
#ifdef _DEBUG
	system::Log::Message("GUI handle created.", system::Log::SUCCESSFUL);
#endif
	m_factoryManager = system::FactoryManager::Create(m_objectManager.get());
#ifdef _DEBUG
	system::Log::Message("Factory manager created.", system::Log::SUCCESSFUL);
#endif
//	m_resourceLoader = system::ResourceLoader::Create();
//#ifdef _DEBUG
//	system::Log::Message("ResourceLoader created.", system::Log::SUCCESSFUL);
//#endif

//	m_sceneSystem = system::SceneSystem::Create();
//#ifdef _DEBUG
//	system::Log::Message("SceneSystem created.", system::Log::SUCCESSFUL);
//#endif
//	m_collisionSystem = system::CollisionSystem::Create();
//#ifdef _DEBUG
//	system::Log::Message("CollisionSystem created.", system::Log::SUCCESSFUL);
//#endif
//
//	world::GameObjectFactory::Initialise(m_objectManager);
//#ifdef _DEBUG
//	system::Log::Message("GameObjectFactory initialised.", system::Log::SUCCESSFUL);
//#endif

	m_gui->Initialise(10.0f, 1280, 720);
	m_objectManager->SetGUIHandle(m_gui.get());

	system::ServiceLocator<system::RenderSystem>::SetService(m_renderSystem.get());
	system::ServiceLocator<system::EventListener>::SetService(m_eventListener.get());
	system::ServiceLocator<system::ObjectManager>::SetService(m_objectManager.get());
	system::ServiceLocator<system::FactoryManager>::SetService(m_factoryManager.get());
	system::ServiceLocator<system::GUI>::SetService(m_gui.get());
	//system::ServiceLocator<system::SceneSystem>::SetService(m_sceneSystem.get());
	//system::ServiceLocator<system::CollisionSystem>::SetService(m_collisionSystem.get());
	//system::ServiceLocator<system::ResourceLoader>::SetService(m_resourceLoader.get());

	//m_sceneSystem->CreateScene("game");


	//TESTS

	m_grid.SetDebugColour(sf::Color::White, sf::Color::Red);
	m_grid.Construct(sf::Vector2f(50, 50), sf::Vector2i(10, 10), sf::Vector2f(10, 10));

	m_grid.FindPath(sf::Vector2f(50, 50), sf::Vector2f(100, 149));

	m_graph = graph(100, 100);
	m_graph.initialise(0.1f);
	m_graph.SetPosition(500, 500);
	m_graph.SetXMax(1000);
	m_graph.SetYMax(10000);
	m_graph.SetColour(sf::Color::Yellow);

	Transform transform;
	transform.position = sf::Vector2f(600, 200);
	transform.rotation.degree = 45;
	transform.scale = sf::Vector2f(100, 100);

	Object* object = m_factoryManager->CreateGUIObject("button", transform);

	auto OnClickFunction = [&]()->void
	{
		printf("OnClick");
	};

	TaskL0<decltype(OnClickFunction)>* l0 = new TaskL0<decltype(OnClickFunction)>(OnClickFunction);

	static_cast<GUIObject*>(object)->SetOnClickFunction(l0);

	return true;
}

void Engine::Update()
{
	UpdateDeltatime();

	m_graph.update(m_deltatime);
	if (m_graph.CanUpdate())
		m_graph.AddValue(1 / m_deltatime);
	
	m_renderSystem->AddToQueue(&m_graph);
	m_renderSystem->AddToQueue(&m_grid);

	m_eventListener->Update();
	m_objectManager->Update(m_renderSystem, m_deltatime);
	m_gui->Update();
	m_renderSystem->Update();
	//m_collisionSystem->Update();
	//m_sceneSystem->Update();
}
void Engine::Cleanup()
{
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/

void Engine::UpdateDeltatime()
{
	//Create a temporary time variable
	sf::Time tempTime = m_clock.getElapsedTime();

	//set the delta time
	m_deltatime = tempTime.asSeconds() - m_previousTime.asSeconds();

	//Change the previous time variable
	m_previousTime = tempTime;
}

/*
----------------------------------------------
protected functions
----------------------------------------------
*/
void Engine::HandleEvent(sf::Event& pEvent)
{
	m_eventListener->HandleEvent(pEvent);
}