#pragma once

#include "RenderComponent.h"
#include "Controller.h"

namespace wh
{
	namespace system
	{
		class Factory
		{
		public:
			Factory();
			~Factory();


		protected:
			virtual Controller* CreateControllerComponent(const char* pComponentName);
			virtual RenderComponent* CreateRenderComponent(const char* pComponentName);

			static unsigned int m_UniqueID;
		};
	}
}