#pragma once

namespace wh
{
	class Object;

	class Component
	{
	public:
		virtual bool Initialise(const char* pName = "component") = 0;
		virtual void Update(float pDeltatime);
		virtual void Cleanup();

		int getHashedID();

		void SetOwner(Object* pOwner);
		void SetParentComponent(Component* pParent);

	protected:
		Object* m_owner;
		Component* m_parent;

		const char* m_name;
		int m_id;
	};
}