#include "stdafx.h"

#include "RenderSystem.h"
#include "ServiceLocator.h"

#include "GUI.h"

namespace wh
{
	namespace system
	{
		GUI::GUI(sf::Window* pWindow)
		{
			m_window = pWindow;
			m_currentObject = nullptr;
		}

		GUI::~GUI()
		{

		}

		GUI::ptr GUI::Create(sf::Window* pWindow)
		{
			return GUI::ptr(new GUI(pWindow));
		}

		bool GUI::Initialise(float pUpdateFrequency, float pWidth, float pHeight)
		{
			pUpdateFrequency; //not implemented

			ChangeScreenSize(pWidth, pHeight);

			return true;
		}

		void GUI::Update()
		{
			sf::Vector2i mousePos = sf::Mouse::getPosition(*ServiceLocator<RenderSystem>::GetService()->getWindow());

			//printf("Mouse position: X: %i Y: %i\n", mousePos.x, mousePos.y);
			if (m_currentObject && m_currentObject->Contains((sf::Vector2f)mousePos))
			{
			}
			else
			{
				if (m_currentObject)
				{
					m_currentObject->ExitHover();
					m_currentObject = nullptr;
				}

				if (m_lastBorder.contains((sf::Vector2f)mousePos))
				{
					CheckBorder(m_lastBorderID, (sf::Vector2f)mousePos);
				}
				else
				{
					if (m_topLeftBorders.contains((sf::Vector2f)mousePos))
					{
						m_lastBorder = m_topLeftBorders;
						m_lastBorderID = TOPLEFT;

						CheckBorder(m_lastBorderID, (sf::Vector2f)mousePos);
					}
					else if (m_topRightBorders.contains((sf::Vector2f)mousePos))
					{
						m_lastBorder = m_topRightBorders;
						m_lastBorderID = TOPRIGHT;

						CheckBorder(m_lastBorderID, (sf::Vector2f)mousePos);
					}
					else if (m_bottomLeftBorders.contains((sf::Vector2f)mousePos))
					{
						m_lastBorder = m_bottomLeftBorders;
						m_lastBorderID = BOTTOMLEFT;

						CheckBorder(m_lastBorderID, (sf::Vector2f)mousePos);
					}
					else if (m_bottomRightBorders.contains((sf::Vector2f)mousePos))
					{
						m_lastBorder = m_bottomRightBorders;
						m_lastBorderID = BOTTOMRIGHT;

						CheckBorder(m_lastBorderID, (sf::Vector2f)mousePos);
					}
				}
			}
		}

		void GUI::Cleanup()
		{
			if (m_window)
				m_window = nullptr;
			if (m_currentObject)
				m_currentObject = nullptr;

			for each (GUIObject* var in m_topLeftElements)
			{
				if (var)
					var = nullptr;
			}
			for each (GUIObject* var in m_topRightElements)
			{
				if (var)
					var = nullptr;
			}
			for each (GUIObject* var in m_bottomLeftElements)
			{
				if (var)
					var = nullptr;
			}
			for each (GUIObject* var in m_bottomRightElements)
			{
				if (var)
					var = nullptr;
			}

		}

		void GUI::ChangeScreenSize(float pWidth, float pHeight)
		{
			m_screenWidth = pWidth;
			m_screenHeight = pHeight;

			//top left
			m_topLeftBorders.top = 0;
			m_topLeftBorders.left = 0;
			m_topLeftBorders.width = pWidth / 2;
			m_topLeftBorders.height = pHeight / 2;

			//top right
			m_topRightBorders.top = 0;
			m_topRightBorders.left = pWidth / 2;
			m_topRightBorders.width = pWidth / 2;
			m_topRightBorders.height = pHeight / 2;

			//bottom left
			m_bottomLeftBorders.top = pHeight / 2;
			m_bottomLeftBorders.left = 0;
			m_bottomLeftBorders.width = pWidth / 2;
			m_bottomLeftBorders.height = pHeight / 2;

			//bottom right
			m_bottomRightBorders.top = pHeight / 2;
			m_bottomRightBorders.left = pWidth / 2;
			m_bottomRightBorders.width = pWidth / 2;
			m_bottomRightBorders.height = pHeight / 2;

			m_lastBorder = m_topLeftBorders;
			m_lastBorderID = TOPLEFT;
		}

		void GUI::AttachGUIObject(GUIObject* pObject)
		{
			if (m_topLeftBorders.contains(pObject->transform.position))
				m_topLeftElements.push_back(pObject);
			else if (m_topRightBorders.contains(pObject->transform.position))
				m_topRightElements.push_back(pObject);
			else if (m_bottomLeftBorders.contains(pObject->transform.position))
				m_bottomLeftElements.push_back(pObject);
			else if (m_bottomRightBorders.contains(pObject->transform.position))
				m_bottomRightElements.push_back(pObject);
		}

		void GUI::MouseButtonPress(sf::Mouse::Button pButton)
		{
			switch (pButton)
			{
			case sf::Mouse::Left:
				if (m_currentObject)
					m_currentObject->OnClick();
				break;
			case sf::Mouse::Right:
				break;
			case sf::Mouse::Middle:
				break;
			case sf::Mouse::XButton1:
				break;
			case sf::Mouse::XButton2:
				break;
			default:
				break;
			}
		}

		bool GUI::CheckBorder(Border pBorder, sf::Vector2f pPosition)
		{
			switch (m_lastBorderID)
			{
			case wh::system::GUI::TOPLEFT:
				for each (GUIObject* var in m_topLeftElements)
				{
					if (var->Contains(pPosition)) //fix var.contains osv
					{
						if (m_currentObject)
							m_currentObject->ExitHover();
						m_currentObject = var;
						m_currentObject->OnHover();
						return true;
					}
				}

				return false;
				break;
			case wh::system::GUI::TOPRIGHT:
				for each (GUIObject* var in m_topRightElements)
				{
					if (var->Contains(pPosition))
					{
						if (m_currentObject)
							m_currentObject->ExitHover();
						m_currentObject = var;
						m_currentObject->OnHover();
						return true;
					}
				}

				return false;
				break;
			case wh::system::GUI::BOTTOMLEFT:
				for each (GUIObject* var in m_bottomLeftElements)
				{
					if (var->Contains(pPosition))
					{
						if (m_currentObject)
							m_currentObject->ExitHover();
						m_currentObject = var;
						m_currentObject->OnHover();
						return true;
					}
				}

				return false;
				break;
			case wh::system::GUI::BOTTOMRIGHT:
				for each (GUIObject* var in m_bottomRightElements)
				{
					if (var->Contains(pPosition))
					{
						if (m_currentObject)
							m_currentObject->ExitHover();
						m_currentObject = var;
						m_currentObject->OnHover();
						return true;
					}
				}

				return false;
				break;
			}
		}

		void GUI::CheckGUIPositions()
		{

		} //Not implemented
	}
}