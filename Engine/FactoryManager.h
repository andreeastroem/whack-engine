#pragma once

#include <memory>
#include "ObjectManager.h"

//factories
#include "GUIFactory.h"

namespace wh
{
	namespace system
	{
		class FactoryManager
		{
			FactoryManager(ObjectManager* pObjectManager);
		public:
			~FactoryManager();

			typedef std::unique_ptr<FactoryManager> ptr;
			static ptr Create(ObjectManager* pObjectManager);

			void Cleanup();

			Object* CreateObject(const char* pObjectType, Transform pTransform = Transform());
			Object* CreateGUIObject(const char* pObjectType, Transform pTransform = Transform());

		private:
			ObjectManager* m_objectManager;

			//factories
			GUIFactory* m_guifactory;
		};
	}
}