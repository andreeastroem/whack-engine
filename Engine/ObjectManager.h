//ObjectManager.h

#pragma once

#include <memory>
#include <string>

#include "RenderSystem.h"
#include "Object.h"
#include "GUI.h"

namespace wh
{
	namespace system
	{
		class CollisionSystem;

		enum ObjectClassification
		{
			GUIOBJECT,
			GAMEOBJECT
		};

		class ObjectManager
		{
			friend class CollisionSystem;
		public:
			typedef std::shared_ptr<ObjectManager> ptr;
			static ptr Create();
			~ObjectManager();

			void AttachObject(Object* pActor, ObjectClassification pType);

			void Update(RenderSystem::ptr& pRenderSystem, float pDeltatime);
			void Cleanup();

			std::vector<Object*> FetchObject(const char* pName);
			Object* FetchObject(unsigned int pUniqueID);
			
			bool SetGUIHandle(GUI* pGUI);

		private:
			ObjectManager();

		public:

		private:
			//Object pool
			std::vector<Object*> m_objects;
			GUI* m_gui;
		};
	}
}

/*
Create empty objects to vector
add their visuals to render queue
update objects
*/