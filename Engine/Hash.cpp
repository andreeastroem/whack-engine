#include "stdafx.h"

#include "Hash.h"

#include <math.h>

namespace wh
{
	namespace util
	{
		int Hash::hash(const char* pString, unsigned int pModulus)
		{
			int bytes = sizeof(pString);

			int asciiChar;
			int hashvalue = 0;

			for (unsigned int i = 0; i < bytes / sizeof(char); i++)
			{
				asciiChar = static_cast<int>(pString[i] * powf(31, (float)i));

				hashvalue += asciiChar;
			}

			hashvalue = hashvalue % pModulus;

			return hashvalue;
		}
	}
}