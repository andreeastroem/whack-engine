/************************************************************************/
/* Logging class for output to console and files
/* Use this class for bool checks that happen once. Such as initialisation
/* and creation, not as a runtime check. Then use printf or equivalent
/************************************************************************/

#pragma once

#define LOGNORMAL 7
#define LOGERROR 12
#define LOGSUCCESSFUL 10

namespace wh
{
	namespace system
	{
		class Log
		{
		public:
			enum EOUTPUTTYPE
			{
				NORMAL = LOGNORMAL,
				ERROR = LOGERROR,
				SUCCESSFUL = LOGSUCCESSFUL
			};

			static void Message(const char* pMessage, EOUTPUTTYPE pColour = NORMAL);
		private:

			static bool WriteToFile();			//NOT YET IMPLEMENTED
			static int GetTime();

		public:

		private:

		};
	}
}