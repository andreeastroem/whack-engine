#include "stdafx.h"

#include "NavigationPath.h"

namespace wh
{

	NavigationPath::NavigationPath(std::vector<NavigationNode*> pPathNodes)
	{
		m_pathNodes = pPathNodes;
	}

	NavigationPath::~NavigationPath()
	{

	}

	bool NavigationPath::Check()
	{
		return true;
	}

	bool NavigationPath::ReCalculate()
	{
		return true;
	}

	sf::Vector2f NavigationPath::TraversePath()
	{
		return sf::Vector2f(0.0f, 0.0f);
	}

}