#include "stdafx.h"

#include "FactoryManager.h"

namespace wh
{
	namespace system
	{

		FactoryManager::FactoryManager(ObjectManager* pObjectManager)
		{
			m_objectManager = pObjectManager;

			m_guifactory = new GUIFactory(m_objectManager);
		}

		FactoryManager::~FactoryManager()
		{
			Cleanup();
		}

		FactoryManager::ptr FactoryManager::Create(ObjectManager* pObjectManager)
		{
			return FactoryManager::ptr(new FactoryManager(pObjectManager));
		}
		
		void FactoryManager::Cleanup()
		{
			if (m_objectManager)
				m_objectManager = nullptr;

			if (m_guifactory)
			{
				m_guifactory->Cleanup();
				delete m_guifactory;
				m_guifactory = nullptr;
			}
		}

		Object* FactoryManager::CreateObject(const char* pObjectType, Transform pTransform)
		{
			return nullptr;
		}

		Object* FactoryManager::CreateGUIObject(const char* pObjectType, Transform pTransform /*= Transform()*/)
		{
			return m_guifactory->CreateGUIObject(pObjectType, pTransform);
		}

	}
}