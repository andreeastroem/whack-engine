#pragma once

#include "component.h"

namespace wh
{
	class ScriptComponent : public Component
	{
	public:
		ScriptComponent();
		~ScriptComponent();

		virtual bool Initialise(const char* pName = "script");

		virtual void Update(float pDeltatime);

		virtual void Cleanup();

	protected:


	};
}