#pragma once

#include <memory>

#include "SFML/Graphics.hpp"

namespace wh
{
	namespace system
	{
		class RenderSystem
		{
		public:
			typedef std::unique_ptr<RenderSystem> ptr;
			static ptr Create(sf::RenderWindow& pWindow);

			~RenderSystem();
			void Update();

			void AddToQueue(sf::Drawable* pObject);
			void AddToQueue(std::vector<sf::Drawable*> pObjects);
			void AddToCameras(sf::View pCamera);

			sf::Window* getWindow();

		private:
			RenderSystem(sf::RenderWindow& pWindow);

			void ClearScreen(sf::Uint8 pRed, sf::Uint8 pGreen, sf::Uint8 pBlue);
			void Draw(sf::Drawable* pObject);
		public:

		private:
			sf::RenderWindow* m_window;
			std::vector<sf::Drawable*> m_renderQueue;
			std::vector<sf::View> m_cameras;
		};
	}
}