#pragma once

#include "SFML/Graphics.hpp"

class debugger
{
public:
	debugger();
	~debugger();

	bool initialise();
	void update();
	void draw(sf::RenderTarget& pTarget);
	
private:
	sf::VertexArray m_vertices;
	
};