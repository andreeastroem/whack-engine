#include "stdafx.h"

#include "graph.h"

graph::graph()
{
	m_vertices.setPrimitiveType(sf::Lines);
	m_dynamic = false;
}

graph::graph(unsigned int pWidth, unsigned int pHeight)
{
	m_vertices.setPrimitiveType(sf::LinesStrip);
	m_width = pWidth;
	m_height = pHeight;

	//text position
	m_XName.setPosition(m_posX - m_width + m_XName.getGlobalBounds().width, m_posY);
	m_YName.setPosition(m_posX, m_posY - m_height + m_YName.getGlobalBounds().height);

	m_dynamic = false;
}

graph::~graph()
{

}

bool graph::initialise(float pUpdateIntervall)
{
	m_updateIntervall = pUpdateIntervall;
	m_currentUpdateDelta = 0.0f;

	if (!m_font.loadFromFile("../resources/fonts/arial.ttf"))
	{
		printf("failed to load font in graph\n");
		return false;
	}

	m_XName.setFont(m_font);
	m_YName.setFont(m_font);

	return true;
}

void graph::update(float pDeltatime)
{
	m_currentUpdateDelta += pDeltatime;

	if (m_currentUpdateDelta >= m_updateIntervall)
	{
		m_currentUpdateDelta -= m_updateIntervall;
		m_canUpdate = true;
	
	#pragma region
		float xDelta;
		if (m_dynamic)
		{
			float pMax = 0.0f;
			for (unsigned int i = 0; i < m_vertices.getVertexCount(); i++)
			{
				if (m_vertices[i].position.y > pMax)
					pMax = m_vertices[i].position.y - m_posY;
			}

			float prevYMax = m_yMax;
			float prevY;

			m_yMax = pMax * prevYMax;

			for (unsigned int i = 0; i < m_vertices.getVertexCount(); i++)
			{
				prevY = m_vertices[i].position.y - m_posY;

				//Continue
			}

		}
		else
		{
			xDelta = m_width / m_xMax;

			for (unsigned int i = 0; i < m_vertices.getVertexCount(); i++)
			{
				m_vertices[i].position.x -= xDelta;
			}
		}
	#pragma endregion
	}
}

void graph::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = &m_texture;

	target.draw(m_vertices, states);

	target.draw(m_XName);
	target.draw(m_YName);
}

void graph::SetXName(sf::String pName)
{
	m_XName.setString(pName);

	m_XName.setPosition(m_posX - m_width + m_XName.getGlobalBounds().width, m_posY);
}

void graph::SetYName(sf::String pName)
{
	m_YName.setString(pName);

	m_YName.setPosition(m_posX, m_posY - m_height + m_YName.getGlobalBounds().height);
}

void graph::SetXMax(float pMax)
{
	m_xMax = pMax;
}

void graph::SetYMax(float pMax)
{
	m_yMax = pMax;	
}

void graph::AddValue(float pValue)
{
	
	if (m_vertices.getVertexCount() >= m_xMax)
	{
		for (unsigned int i = 0; i < m_vertices.getVertexCount() - 1; i++)
		{
			m_vertices[i] = m_vertices[i + 1];
		}

		float height = pValue / m_yMax;

		m_vertices[m_vertices.getVertexCount() - 1].position = sf::Vector2f((0) + m_posX, -(height * m_height) + m_posY);
	}
	else
	{
		sf::Vertex value;

		float height = pValue / m_yMax;

		value.position = sf::Vector2f(0 + m_posX, -(height * m_height) + m_posY);
		value.color = m_pointColour;

		m_vertices.append(value);
	}

	m_canUpdate = false;
	
	//printf("Graph value: %0.2f\n", pValue);

	
}

void graph::SetDynamic(bool pState)
{
	m_dynamic = pState;
}

void graph::SetColour(sf::Color pColour)
{
	m_pointColour = pColour;
}

void graph::SetPosition(float pPosX, float pPosY)
{
	m_posX = pPosX;
	m_posY = pPosY;
}

bool graph::CanUpdate()
{
	return m_canUpdate;
}
