#include "stdafx.h"

#include "GUIObject.h"

#include "RenderComponent.h"

namespace wh
{

	GUIObject::GUIObject()
	{
		m_bounds = sf::FloatRect();
	}

	GUIObject::GUIObject(unsigned int pUniqueID)
	{
		m_UniqueID = pUniqueID;
	}

	GUIObject::~GUIObject()
	{

	}

	bool GUIObject::Initialise(sf::Vector2f pPosition /*= sf::Vector2f(0.0f, 0.0f)*/)
	{
		__super::Initialise(pPosition);
		
		return true;
	}

	void GUIObject::Update(float pDeltatime)
	{
		__super::Update(pDeltatime);
		
	}

	void GUIObject::Cleanup()
	{
		__super::Cleanup();
	}

	void GUIObject::OnClick()
	{
		m_script->OnClick();
	}

	void GUIObject::OnHover()
	{
		m_script->OnHover();
	}

	void GUIObject::ExitHover()
	{
		m_script->ExitHover();
	}

	bool GUIObject::IsDirty()
	{
		return m_dirty;
	}

	void GUIObject::AttachComponent(Component* pComponent)
	{
		__super::AttachComponent(pComponent);
		
		if (pComponent->getHashedID() == util::Hash::hash("script"))
		{
			m_script = static_cast<GUIScriptComponent*>(pComponent);
		}
		else if (pComponent->getHashedID() == util::Hash::hash("render"))
		{
			m_bounds = static_cast<RenderComponent*>(pComponent)->GetBounds();
		}
	}

	void GUIObject::SetOnClickFunction(TaskBase* pTask)
	{
		m_script->SetOnClickFunction(pTask);
	}

	bool GUIObject::Contains(sf::Vector2f pPosition)
	{
		if (m_bounds.contains(pPosition))
			return true;
		return false;
	}

}