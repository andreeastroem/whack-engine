#pragma once

#include <functional>

namespace wh
{
	class TaskBase
	{
	public:
		virtual void Run(){};
	};

	class TaskS0 : public TaskBase
	{
	public:
		typedef void(*FncType)();
		FncType m_fnc;
		virtual void Run()
		{
			(*m_fnc)();
		}
		TaskS0(FncType pFnc) : m_fnc(pFnc){};
	};

	template <typename TArg0>
	class TaskS1 : public TaskBase
	{
	public:
		typedef void(*FncType)(TArg0);
		FncType m_fnc;
		TArg0 m_arg0;
		virtual void Run()
		{
			(*m_fnc)(m_arg0);
		}
		TaskS1(FncType pFnc, TArg0 pArg0) : m_fnc(pFnc), pArg0(m_arg0){};
	};

	template <typename TObj>
	class TaskM0 : public TaskBase
	{
	public:
		typedef void(TObj::*FncType)();
		FncType m_fnc;
		TObj* m_obj;
		virtual void Run(){ (m_obj->*m_fnc)(); };
		TaskM0(TObj* pObj, FncType pFnc) :m_obj(pObj), m_fnc(pFnc){};
	};

	template <typename TObj, typename TArg0>
	class TaskM1 : public TaskBase
	{
	public:
		typedef void(TObj::*FncType)(TArg0);
		FncType m_fnc;
		TObj* m_obj;
		TArg0 m_arg0;
		virtual void Run(){ (m_obj->*m_fnc)(m_arg0); };
		TaskM1(TObj* pObj, FncType pFnc, TArg0 pArg0) :m_obj(pObj), m_fnc(pFnc), m_arg0(pArg0){};
	};

	template < typename TObj, typename TArg0, typename TArg1 >
	class TaskM2 : public TaskBase
	{
	public:
		typedef void(TObj::*FncType)(TArg0, TArg1);
		FncType m_fnc;
		TObj* m_obj;
		TArg0 m_arg0;
		TArg1 m_arg1;
		virtual void Run(){ (m_obj->*m_fnc)(m_arg0, m_arg1); };
		TaskM2(TObj* pObj, FncType pFnc, TArg0 pArg0, TArg1 pArg1) :m_obj(pObj), m_fnc(pFnc), m_arg0(pArg0), m_arg1(pArg1){};
	};

	template <typename TFunc>
	class TaskL0 : public TaskBase
	{
	public:
		TFunc m_func;
		TaskL0(TFunc p_f) :m_func(p_f){};
		virtual void Run(){ m_func(); };
	};

	template <typename TFunc, typename TArg0>
	class TaskL1 : public TaskBase
	{
	public:
		TFunc m_func;
		TArg0 m_arg0;
		TaskL1(TFunc pFunc, TArg0 pArg0) :m_func(pFunc), m_arg0(pArg0){};
		virtual void Run(){ m_func(m_arg0); };
	};
}