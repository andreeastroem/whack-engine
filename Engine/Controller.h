#pragma once

#include "component.h"

namespace wh
{
	class Controller : public Component
	{
	public:
		Controller();
		~Controller();

		virtual bool Initialise(const char* pName = "controller") override;
		virtual void Update(float pDeltatime) override;
		virtual void Cleanup() override;

	private:

	};
}