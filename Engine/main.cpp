// Project Y.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include "Application.h"


int _tmain(int argc, _TCHAR* argv[])
{
	Application app;

	if (app.Initialise())
	{
		app.Run();
	}

	app.Cleanup();

	return 0;
}

