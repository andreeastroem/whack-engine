#pragma once

namespace wh
{
	namespace util
	{
		class Hash
		{
		public:
			/*
			 * hashing function with an optional modulus value (set at 256 from start)
			 **/
			static int hash(const char* pString, unsigned int pModulus = 256);
		private:
		};
	}
}