/************************************************************************/
/* Class for rendering rectangles
/************************************************************************/

#pragma once

#include "RenderComponent.h"

namespace wh
{
	
	class RectangleComponent : public RenderComponent
	{
	public:

		RectangleComponent();
		~RectangleComponent();

		bool Initialise(sf::Vector2f pPosition, sf::Vector2f pSize, sf::Color pColour = sf::Color::White, const char* pName = "render");

		//Inherited functions
		sf::Drawable* GetRenderData();
		void Cleanup();
		void Update();

		void SetColour(sf::Color pColour);
		sf::Color GetColour();
		virtual sf::FloatRect GetBounds() override;

	private:
	protected:
		sf::RectangleShape* m_shape;
	};
	
}