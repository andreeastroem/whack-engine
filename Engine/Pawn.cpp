#include "stdafx.h"

#include "Pawn.h"

namespace wh
{

	Pawn::Pawn()
	{

	}

	Pawn::~Pawn()
	{

	}

	bool Pawn::Initialise(sf::Vector2f pPosition)
	{
		throw std::logic_error("The method or operation is not implemented.");
	}

	void Pawn::Update(float pDeltatime)
	{
		__super::Update(pDeltatime);
		

	}

	void Pawn::Cleanup()
	{
		__super::Cleanup();
		
		if (m_controller)
		{
			delete m_controller;
			m_controller = nullptr;
		}
	}

}