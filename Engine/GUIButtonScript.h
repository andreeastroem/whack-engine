#pragma once

#include "GUIScriptComponent.h"
#include "RenderComponent.h"

namespace wh
{
	class GUIButtonScript : public GUIScriptComponent
	{
	public:
		GUIButtonScript();
		~GUIButtonScript();

		virtual void OnClick() override;
		virtual void OnHover() override;
		virtual void ExitHover() override;

		virtual bool Initialise(const char* pName = "script") override;
		virtual void Update(float pDeltatime) override;
		virtual void Cleanup() override;

		virtual void SetOnClickFunction(TaskBase* pTask) override;

	private:
		RenderComponent* m_rendercomp;
	};
}