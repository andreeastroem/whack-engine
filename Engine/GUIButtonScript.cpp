#include "stdafx.h"

#include "GUIButtonScript.h"

#include "Object.h"

namespace wh
{
	GUIButtonScript::GUIButtonScript()
	{
		
	}
	GUIButtonScript::~GUIButtonScript()
	{

	}

	void GUIButtonScript::OnClick()
	{
		if (m_task)
			m_task->Run();
	}

	void GUIButtonScript::OnHover()
	{
		sf::Color colour = m_rendercomp->GetColour();
		//colour = sf::Color(colour.r * -1, colour.g * -1, colour.b * -1);
		colour.a /= 2;
		m_rendercomp->SetColour(colour);
	}

	void GUIButtonScript::ExitHover()
	{
		sf::Color colour = m_rendercomp->GetColour();
		//colour = sf::Color(colour.r * -1, colour.g * -1, colour.b * -1);
		colour.a *= 2;
		m_rendercomp->SetColour(colour);
	}

	bool GUIButtonScript::Initialise(const char* pName /*= "script"*/)
	{
		__super::Initialise(pName);
		
		m_rendercomp = m_owner->GetComponent<RenderComponent>("render");

		if (!m_rendercomp)
			return false;

		return true;
	}

	void GUIButtonScript::Update(float pDeltatime)
	{

	}

	void GUIButtonScript::Cleanup()
	{

	}

	void GUIButtonScript::SetOnClickFunction(TaskBase* pTask)
	{
		__super::SetOnClickFunction(pTask);
		
	}

}