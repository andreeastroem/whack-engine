#pragma once

#include "Object.h"

#include "GUIScriptComponent.h"

#include "Task.h"

namespace wh
{
	class GUIObject : public Object
	{
	public:
		GUIObject();
		GUIObject(unsigned int pUniqueID);
		~GUIObject();

		virtual bool Initialise(sf::Vector2f pPosition = sf::Vector2f(0.0f, 0.0f)) override;
		virtual void Update(float pDeltatime) override;
		virtual void Cleanup() override;

		virtual void OnClick();
		virtual void OnHover();
		virtual void ExitHover();
		virtual bool Contains(sf::Vector2f pPoint);

		bool IsDirty();

		virtual void AttachComponent(Component* pComponent) override;
		void SetOnClickFunction(TaskBase* pTask);

	protected:
		bool m_dirty;
		sf::FloatRect m_bounds;

		GUIScriptComponent* m_script;
	};
}