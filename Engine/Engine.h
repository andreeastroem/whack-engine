//Engine.h

#pragma once

#include "RenderSystem.h"
#include "EventListener.h"
#include "ObjectManager.h"
#include "GUI.h"
#include "FactoryManager.h"
//#include "AudioSystem.h"
//#include "SceneSystem.h"
//#include "CollisionSystem.h"
//#include "ResourceLoader.h"
class Application;

#include "Log.h"
#include "Hash.h"

//test och visning
#include "graph.h"
#include "NavigationGrid.h"

namespace wh
{
	namespace system
	{
		class EventListener;
	}
	class Engine
	{
		friend class Application;
	public:
		Engine();
		~Engine();

		bool Initialise(sf::RenderWindow& pWindow);
		void Update();
		void Cleanup();

	private:
		void UpdateDeltatime();

	public:
	private:
		system::RenderSystem::ptr m_renderSystem;
		system::EventListener::ptr m_eventListener;
		system::ObjectManager::ptr m_objectManager;
		system::GUI::ptr m_gui;
		//system::SceneSystem::ptr m_sceneSystem;
		//system::CollisionSystem::ptr m_collisionSystem;
		//system::ResourceLoader::ptr m_resourceLoader;
		system::FactoryManager::ptr m_factoryManager;

		sf::Clock m_clock;
		sf::Time m_previousTime;
		float m_deltatime = 0.0f;

		//test och visning
		NavigationGrid m_grid;
		graph m_graph;

	protected:
		void HandleEvent(sf::Event& pEvent);
	};
}