#include "stdafx.h"

#include "Arrow.h"

namespace sf
{
	arrow::arrow(Vector2f pSize)
	{
		m_size = pSize;
		update();
	}

	arrow::~arrow()
	{

	}

	
	std::size_t arrow::getPointCount() const
	{
		return 4;
	}

	sf::Vector2f arrow::getPoint(std::size_t pIndex) const
	{
		unsigned int index = pIndex % 4;

		switch (index)
		{
		case 0:
			return Vector2f(-m_size.x/2, m_size.y/2);
			break;
		case 1:
			return Vector2f(0, -m_size.y/2);
			break;
		case 2:
			return Vector2f(m_size.x/2, m_size.y/2);
			break;
		case 3:
			return Vector2f();
			break;
		}

		return Vector2f();
	}

}


