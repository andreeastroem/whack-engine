#pragma once

#include "Object.h"
#include "Controller.h"


namespace wh
{
	class Pawn : public Object
	{
	public:
		Pawn();
		~Pawn();

		virtual bool Initialise(sf::Vector2f pPosition = sf::Vector2f(0.0f, 0.0f)) override;
		virtual void Update(float pDeltatime) override;
		virtual void Cleanup() override;


	private:
		Controller* m_controller;
	};
}