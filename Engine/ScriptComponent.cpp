#include "stdafx.h"

#include "ScriptComponent.h"

namespace wh
{

	ScriptComponent::ScriptComponent()
	{

	}

	ScriptComponent::~ScriptComponent()
	{

	}

	bool ScriptComponent::Initialise(const char* pName /*= "component"*/)
	{
		__super::Initialise(pName);
		
		return true;
	}

	void ScriptComponent::Update(float pDeltatime)
	{
	}

	void ScriptComponent::Cleanup()
	{
	}

}