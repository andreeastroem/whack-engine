#pragma once

#include "NavigationNode.h"
#include "SFML/System/Vector2.hpp"
#include <vector>


namespace wh
{
	class NavigationPath
	{
	public:
		NavigationPath(std::vector<NavigationNode*> pPathNodes);
		~NavigationPath();

		bool Check();
		sf::Vector2f TraversePath();

	private:
		bool ReCalculate();

		std::vector<NavigationNode*> m_pathNodes;
	};
}