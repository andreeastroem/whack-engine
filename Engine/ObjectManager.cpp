//ObjectManager.cpp

#include "stdafx.h"

//#include "CollisionSystem.h"

#include "ObjectManager.h"

#include "RenderComponent.h"
//#include "TextComponent.h"

//#include "Camera2D.h"

#include "Log.h"

namespace wh
{
	namespace system
	{
		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		ObjectManager::ObjectManager()
		{
		}
		ObjectManager::~ObjectManager()
		{
			Cleanup();
		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		ObjectManager::ptr ObjectManager::Create()
		{
			return ObjectManager::ptr(new ObjectManager());
		}
		void ObjectManager::Update(RenderSystem::ptr& pRenderSystem, float pDeltatime)
		{
			if (m_objects.size() == 0)
			{
				Log::Message("Empty object manager");
			}
			for (int i = (m_objects.size() - 1); i >= 0; i--)
			{
				m_objects[i]->Update(pDeltatime);

				if (m_objects[i]->GetDestroyed())
				{
					m_objects[i]->Cleanup();
					delete m_objects[i];
					m_objects[i] = nullptr;
					m_objects.erase(m_objects.begin() + i);
				}
				else
				{
					//if object has visuals
					//add those to the queue
					RenderComponent* render = m_objects[i]->GetComponent<RenderComponent>("render");
					if (render)
					{
						sf::Drawable* visual = render->GetRenderData();
						pRenderSystem->AddToQueue(visual);
					}
				}
			}


		}
		std::vector<Object*> ObjectManager::FetchObject(const char* pName)
		{
			std::vector<Object*> foundObjects;
			for (unsigned int i = 0; i < m_objects.size(); i++)
			{
				if (m_objects[i]->name == pName)
					foundObjects.push_back(m_objects[i]);
			}

			return foundObjects;
		}
		Object* ObjectManager::FetchObject(unsigned int pUniqueID)
		{
			for each (Object* var in m_objects)
			{
				if (var->GetUniqueID() == pUniqueID)
					return var;
			}

			Log::Message("Object X was not found.", Log::ERROR);

			return nullptr;
		}

		bool ObjectManager::SetGUIHandle(GUI* pGUI)
		{
			if (!m_gui)
			{
				m_gui = pGUI;
				return true;
			}
			else
				return false;
		}

		void ObjectManager::Cleanup()
		{
			for (unsigned int i = 0; i < m_objects.size(); i++)
			{
				m_objects[i]->Cleanup();
				delete m_objects[i];
				m_objects[i] = nullptr;
			}
		}
		void ObjectManager::AttachObject(Object* pActor, ObjectClassification pType)
		{
			if (pType == GAMEOBJECT)
				m_objects.push_back(pActor);
			else if (pType == GUIOBJECT)
			{
				m_objects.push_back(pActor);
				m_gui->AttachGUIObject(static_cast<GUIObject*>(pActor));
			}
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/
	}
}
