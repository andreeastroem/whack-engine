#include "stdafx.h"

#include "Factory.h"

#include "Hash.h"
#include "Log.h"

//standard components
#include "RectangleComponent.h"

namespace wh
{
	namespace system
	{

		Factory::Factory()
		{

		}

		Factory::~Factory()
		{

		}

		Controller* Factory::CreateControllerComponent(const char* pComponentName)
		{
			int id = util::Hash::hash(pComponentName);

			Controller* controller = nullptr;

			switch (id)
			{
				//player controller
			case 140:
				break;
			default:
				Log::Message("No predefined component type with such name.", Log::ERROR);
				break;
			}

			return controller;
		}

		RenderComponent* Factory::CreateRenderComponent(const char* pComponentName)
		{
			int id = util::Hash::hash(pComponentName);

			RenderComponent* rendercomponent = nullptr;

			switch (id)
			{
				//rectangle
			case 92:
				rendercomponent = new RectangleComponent();
				
				break;
				//Sprite
			case 236:

				break;
				//triangle
			case 106:

				break;
				//text
			case 51:

				break;
			default:
				Log::Message("No predefined component type with such name.", Log::ERROR);
				break;
			}

			return rendercomponent;
		}

		unsigned int Factory::m_UniqueID = 0;

	}
}