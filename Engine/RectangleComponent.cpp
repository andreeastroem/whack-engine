
#include "stdafx.h"

#include "SFML/Graphics.hpp"

#include "RectangleComponent.h"

#include "Hash.h"

namespace wh
{
		/************************************************************************/
		/* Constructors and deconstructors
		/************************************************************************/
		RectangleComponent::RectangleComponent()
		{
			m_shape = new sf::RectangleShape;
		}

		RectangleComponent::~RectangleComponent()
		{
			Cleanup();
		}

		bool RectangleComponent::Initialise(sf::Vector2f pPosition, sf::Vector2f pSize, sf::Color pColour, const char* pName)
		{
			m_shape->setSize(pSize);
			m_shape->setOrigin(pSize.x / 2, pSize.y / 2);
			m_shape->setFillColor(pColour);
			m_shape->setPosition(pPosition);

			m_name = pName;

			m_id = util::Hash::hash(pName);

			return true;
		}

		/************************************************************************/
		/* Inherited functions
		/************************************************************************/
		sf::Drawable* RectangleComponent::GetRenderData()
		{
			return m_shape;
		}

		void RectangleComponent::Cleanup()
		{
			if (m_shape)
			{
				delete m_shape;
				m_shape = nullptr;
			}
		}

		void RectangleComponent::Update()
		{
		}

		void RectangleComponent::SetColour(sf::Color pColour)
		{
			m_shape->setFillColor(pColour);
		}

		sf::Color RectangleComponent::GetColour()
		{
			return m_shape->getFillColor();
		}

		sf::FloatRect RectangleComponent::GetBounds()
		{
			return m_shape->getGlobalBounds();
		}

}