//RenderComponent.cpp

#include "stdafx.h"

#include "SFML/Graphics.hpp"

#include "RenderComponent.h"
#include "EventListener.h"


namespace wh
{
	/************************************************************************/
	/* Constructors and deconstructor
	/************************************************************************/
	RenderComponent::RenderComponent()
	{

	}

	RenderComponent::~RenderComponent()
	{

	}

	/************************************************************************/
	/* Inherited functions
	/************************************************************************/

	bool RenderComponent::Initialise(const char* pName)
	{
		__super::Initialise(pName);
		
		return true;
	}

	void RenderComponent::Update(float pDeltatime)
	{

	}

	void RenderComponent::Cleanup()
	{
		__super::Cleanup();
		
	}

	/************************************************************************/
	/* Virtual classes
	/************************************************************************/
	RenderType RenderComponent::GetRenderType()
	{
		return m_type;
	}

	void RenderComponent::SetColour(sf::Color pColour)
	{
		(void)pColour;
	}
}