#pragma once

#include "NavigationNode.h"
#include "SFML/Graphics.hpp"
#include <vector>

namespace wh
{
	class NavigationPath;

	class NavigationGrid : public sf::Drawable
	{
		struct weightedNavigationNode
		{
			weightedNavigationNode(NavigationNode* pNode, float p_f, float p_g, int pH, weightedNavigationNode* pParent = nullptr);
			bool operator>(weightedNavigationNode pNode);
			bool operator<(weightedNavigationNode pNode);

			weightedNavigationNode* m_parent;
			NavigationNode* m_node;
			float m_f, m_g;
			int m_h;
		};
	public:
		NavigationGrid();
		~NavigationGrid();

		bool Construct(sf::Vector2f pStartPosition, sf::Vector2i pSize, sf::Vector2f pNodeInterval);
		void AlterNode(sf::Vector2f pPosition, Object* pOccupant, bool pWalkableState = false);
		void Clean();
		
		NavigationPath* FindPath(sf::Vector2f pStart, sf::Vector2f pTarget);

		bool NodeWithinBounds(int x, int y);

		void SetDebugColour(sf::Color pNodeColour, sf::Color pPathFindingColour);

		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		NavigationNode* getNodeAtPosition(sf::Vector2f pPosition);

	private:
		sf::Vertex& getDebugNodeAtPosition(sf::Vector2f pPosition);

		sf::Vector2f m_startposition;
		sf::Vector2i m_size;
		sf::Vector2f m_nodeInterval;

		std::vector<std::vector<NavigationNode*>> m_grid;


		/************************************************************************/
		/* Debugging
		/************************************************************************/
		//nodes
		sf::VertexArray m_nodeVertices;
		sf::Color m_nodeColour;

		//path finding
		sf::VertexArray m_pathFindingVertices;
		sf::Color m_pathFindingColour;
	};
}