#pragma once

#include "GUIObject.h"

#include <memory>

#include <vector>

namespace wh
{
	namespace system
	{
		class GUI
		{
			enum Border
			{
				TOPLEFT,
				TOPRIGHT,
				BOTTOMLEFT,
				BOTTOMRIGHT
			};
			GUI(sf::Window* pWindow);
		public:
			~GUI();
			typedef std::unique_ptr<GUI> ptr;
			static ptr Create(sf::Window* pWindow);

			bool Initialise(float pUpdateFrequency, float pWidth, float pHeight);
			void Update();
			void Cleanup();

			void ChangeScreenSize(float pWidth, float pHeight);

			void AttachGUIObject(GUIObject* pObject);

			void MouseButtonPress(sf::Mouse::Button pButton);

		private:
			bool CheckBorder(Border pBorder, sf::Vector2f pPosition);
			void CheckGUIPositions();

			sf::Window* m_window;

			std::vector<GUIObject*> m_topLeftElements;
			std::vector<GUIObject*> m_topRightElements;
			std::vector<GUIObject*> m_bottomLeftElements;
			std::vector<GUIObject*> m_bottomRightElements;

			float m_screenWidth;
			float m_screenHeight;

			sf::FloatRect m_topLeftBorders;
			sf::FloatRect m_topRightBorders;
			sf::FloatRect m_bottomLeftBorders;
			sf::FloatRect m_bottomRightBorders;

			GUIObject* m_currentObject;
			sf::FloatRect m_lastBorder;
			Border m_lastBorderID;
		};
	}
}