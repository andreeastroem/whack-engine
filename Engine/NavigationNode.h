#pragma once

#include "SFML/System/Vector2.hpp"

namespace wh
{
	class Object;

	class NavigationNode
	{
	public:
		enum restriction
		{
			WATERWALKING,
			CLIFFWALKING
		};
	public:
		NavigationNode(sf::Vector2f pPosition, sf::Vector2i pGridIndex, bool pWalkable = true, float pTerrainToughness = 0.0f);
		~NavigationNode();

		void Occupy(Object* pObject, bool pWalkable = false);
		void Leave();

		Object* getOccupant();

		bool isWalkable();

		void subscribe();
		void unsubscribe();
		bool getDirty();

		sf::Vector2f getPosition();
		sf::Vector2i getGridIndex();

		float GetTerrainToughness();

	private:
		sf::Vector2i m_gridIndex;
		sf::Vector2f m_position;

		bool m_walkable;
		float m_terrainToughness;

		NavigationNode* m_parent;

		Object* m_occupant;

		int m_pathCount;
		bool m_dirty;
	};
}