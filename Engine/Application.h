#pragma once

#include "SFML/Graphics.hpp"

namespace wh
{
	class Engine;
}

class Application
{
public:
	Application();
	~Application();

	bool Initialise();
	void Run();
	void Cleanup();

private:
	bool ReadConfig();
	void HandleEvent(sf::Event& pEvent);
public:

private:
	int m_windowHeight, m_windowWidth, m_windowAA;
	sf::RenderWindow m_window;
	sf::ContextSettings m_windowSettings;
	sf::String m_title;

	wh::Engine* m_engine;
};