#pragma once

#include "SFML/Graphics.hpp"

#include "component.h"

#include <vector>

#include "Hash.h"

namespace wh
{
	struct Rotation
	{
		float degree;
		float radian;

	public:
		Rotation()
		{
			degree = 0.0f;
			radian = 0.0f;
		}
	};
	//Fixa s� att scale inte r�knas som size l�ngre utan att det �r en egen variabel
	struct Transform
	{
		sf::Vector2f position;
		Rotation rotation;
		sf::Vector2f scale;

		Transform()
		{
			position = sf::Vector2f();
			rotation = Rotation();
			scale = sf::Vector2f();
		}
	};

	class Object
	{
	public:
		Object();
		Object(unsigned int pUniqueID);
		~Object();

		virtual bool Initialise(sf::Vector2f pPosition = sf::Vector2f(0.0f, 0.0f)){ return true; };
		virtual void Update(float pDeltatime);
		virtual void Cleanup();

		void Destroy();
		bool GetDestroyed();

		unsigned int GetUniqueID();

		virtual void AttachComponent(Component* pComponent);
		bool HasComponent(const char* pComponentName);

		template <class T>
		T* GetComponent(const char* pComponentName);

	public:
		const char* name;
		Transform transform;
	protected:
		//std::vector<Component*> m_components;
		std::map<int, Component*> m_components;
		bool m_isDestroyed = false;
		unsigned int m_UniqueID;
	};

	template <class T>
	T* Object::GetComponent(const char* pComponentName)
	{
		auto it = m_components.find(util::Hash::hash(pComponentName));

		if (it != m_components.end())
			return static_cast<T*>(it->second);
		return nullptr;
	}
}
