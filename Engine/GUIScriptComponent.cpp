#include "stdafx.h"

#include "GUIScriptComponent.h"

namespace wh
{

	GUIScriptComponent::GUIScriptComponent()
	{

	}

	GUIScriptComponent::~GUIScriptComponent()
	{

	}

	void GUIScriptComponent::OnClick()
	{
		if (m_task)
			m_task->Run();
	}

	void GUIScriptComponent::OnHover()
	{

	}

	void GUIScriptComponent::ExitHover()
	{

	}

	bool GUIScriptComponent::Initialise(const char* pName /*= "script"*/)
	{
		__super::Initialise(pName);
		

		return true;
	}

	void GUIScriptComponent::Update(float pDeltatime)
	{
	}

	void GUIScriptComponent::Cleanup()
	{
		__super::Cleanup();
		
		if (m_task)
		{
			delete m_task;
			m_task = nullptr;
		}
	}

	void GUIScriptComponent::SetOnClickFunction(TaskBase* pTask)
	{
		m_task = pTask;
	}

}