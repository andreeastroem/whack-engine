//EventListener.h

#pragma once

#include <unordered_map>
#include <memory>

namespace wh
{
	class Engine;
	namespace system
	{
		class EventListener
		{
			friend class Engine;
		public:
			typedef std::unique_ptr<EventListener> ptr;
			static ptr Create();

			void Update();

			//Managed automaticly
			~EventListener();
		private:
			EventListener();

			void HandleEvent(sf::Event& pEvent);

		};
	}
}