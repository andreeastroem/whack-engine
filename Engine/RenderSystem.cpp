#include "stdafx.h"

#include "RenderSystem.h"

namespace wh
{
	namespace system
	{
		/*
		----------------------------------------------
		Constructors and deconstructors
		----------------------------------------------
		*/
		RenderSystem::RenderSystem(sf::RenderWindow& pWindow)
		{
			m_window = &pWindow;
		}
		RenderSystem::~RenderSystem()
		{
			m_window = nullptr;
		}

		/*
		----------------------------------------------
		public functions
		----------------------------------------------
		*/
		RenderSystem::ptr RenderSystem::Create(sf::RenderWindow& pWindow)
		{
			return RenderSystem::ptr(new RenderSystem(pWindow));
		}
		void RenderSystem::Update()
		{
			ClearScreen(0x00, 0x00, 0x00);

			if (m_cameras.size() == 0)
			{
				m_window->setView(m_window->getDefaultView());

				for each (sf::Drawable* var in m_renderQueue)
				{
					if (var)
						Draw(var);
				}
			}
			else
			{
				for (unsigned int j = 0; j < m_cameras.size(); j++)
				{
					m_window->setView(m_cameras[j]);
					for (unsigned int i = 0; i < m_renderQueue.size(); i++)
					{
						if (m_renderQueue[i])
							Draw(m_renderQueue[i]);
					}
				}
			}

			m_window->display();

			m_renderQueue.clear();
			m_cameras.clear();
		}
		void RenderSystem::AddToQueue(sf::Drawable* pObject)
		{
			if (pObject)
				m_renderQueue.push_back(pObject);
		}
		void RenderSystem::AddToQueue(std::vector<sf::Drawable*> pObjects)
		{
			for (unsigned int i = 0; i < pObjects.size(); i++)
			{
				if (pObjects[i])
					m_renderQueue.push_back(pObjects[i]);
			}
		}

		/*
		----------------------------------------------
		private functions
		----------------------------------------------
		*/
		void RenderSystem::ClearScreen(sf::Uint8 pRed, sf::Uint8 pGreen, sf::Uint8 pBlue)
		{
			m_window->clear(sf::Color(pRed, pGreen, pBlue));
		}
		void RenderSystem::Draw(sf::Drawable* pObject)
		{
			m_window->draw(*pObject);
		}

		void RenderSystem::AddToCameras(sf::View pCamera)
		{
			m_cameras.push_back(pCamera);
		}

		sf::Window* RenderSystem::getWindow()
		{
			return m_window;
		}

	}
}