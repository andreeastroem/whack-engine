//Application.cpp

#include "stdafx.h"

#include "Engine.h"

#include "Application.h"

#include <fstream>

using namespace wh;

/*
----------------------------------------------
Constructors and deconstructors
----------------------------------------------
*/
Application::Application()
{

}

Application::~Application()
{
	Cleanup();
}

/*
----------------------------------------------
public functions
----------------------------------------------
*/
bool Application::Initialise()
{
	ReadConfig();

	m_windowSettings.antialiasingLevel = m_windowAA;
	m_window.create(sf::VideoMode(m_windowWidth, m_windowHeight),
		m_title, sf::Style::Default, m_windowSettings);

	m_window.setJoystickThreshold(40);

	m_engine = new Engine;
	if (!m_engine->Initialise(m_window))
		return false;

	return true;
}
void Application::Run()
{
	//game loop
	while (m_window.isOpen())
	{
		//event handling
		sf::Event event;
		while (m_window.pollEvent(event))
		{
			HandleEvent(event);
		}

		m_engine->Update();
	}
}
void Application::Cleanup()
{
	if (m_engine != nullptr)
	{
		m_engine->Cleanup();
		delete m_engine;
		m_engine = nullptr;
	}
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/

bool Application::ReadConfig()
{
	int counter = 0;
	std::ifstream stream;
	stream.open("../resources/config/WindowConfig.txt");

	if (!stream.is_open())
		return false;

	while (!stream.eof())
	{
		std::string line;
		std::getline(stream, line, '\n');

		//empty lines
		if (line.length() == 0)
			continue;
		//comments
		std::size_t pos = line.find('#');
		if (pos != std::string::npos)
			line.erase(pos);
		//Erase spaces
		if (line.length() > 0)
			line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

		//Find essential variables
		pos = line.find(':');
		if (pos != std::string::npos)
		{
			if (line.substr(0, pos) == "title")
			{
				m_title = line.substr(pos + 1);
				counter++;
			}
			else if (line.substr(0, pos) == "width")
			{
				m_windowWidth = std::stoi(line.substr(pos + 1));
				counter++;
			}
			else if (line.substr(0, pos) == "height")
			{
				m_windowHeight = std::stoi(line.substr(pos + 1));
				counter++;
			}
			else if (line.substr(0, pos) == "AA")
			{
				m_windowAA = std::stoi(line.substr(pos + 1));
				counter++;
			}
		}
	}

	if (counter < 4)
	{
		stream.close();
		return false;
	}
	stream.close();

	return true;
}

void Application::HandleEvent(sf::Event& pEvent)
{
	switch (pEvent.type)
	{
	case sf::Event::Closed:
		m_window.close();
		break;
	case sf::Event::KeyPressed:
		if (pEvent.key.code == sf::Keyboard::Escape)
			m_window.close();
		if (pEvent.key.code == sf::Keyboard::Delete)
			::system("cls");
	default:
		m_engine->HandleEvent(pEvent);
		break;
	}
}