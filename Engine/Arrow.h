#pragma once

#include "SFML/Graphics.hpp"

namespace sf
{
	class arrow : public Shape
	{
	public:
		
		arrow(Vector2f pSize = Vector2f(5, 5));
		~arrow();

		virtual std::size_t getPointCount() const override;

		virtual Vector2f getPoint(std::size_t index) const override;

	private:
		Vector2f m_size;
	};
}
