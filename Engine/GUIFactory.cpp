#include "stdafx.h"

#include "GUIFactory.h"

//necessary components
#include "RectangleComponent.h"

#include "GUIButtonScript.h"

namespace wh
{
	namespace system
	{
		
		GUIFactory::GUIFactory(ObjectManager* pObjectManager)
		{
			m_objectManager = pObjectManager;
		}

		GUIFactory::~GUIFactory()
		{

		}

		void GUIFactory::Cleanup()
		{
			if (m_objectManager)
				m_objectManager = nullptr;
		}

		GUIObject* GUIFactory::CreateGUIObject(const char* pName, Transform pTransform)
		{
			GUIObject* object = new GUIObject(m_UniqueID);
			m_UniqueID++;
			//switcherino
			switch (pName[0])
			{
			case 'B':
			case 'b':
				CreateButton(pTransform.position.x, pTransform.position.y, pTransform.scale.x, pTransform.scale.y, *object);
				break;
			default:
				object = nullptr;
				break;
			}

			return object;
		}

		bool GUIFactory::CreateTextField()
		{
			return true;
		}

		bool GUIFactory::CreateButton(float x, float y, float width, float height, GUIObject& pObject, const char* pLabel /*= "button"*/)
		{
			GUIObject* button = new GUIObject();

			RectangleComponent* render = static_cast<RectangleComponent*>(CreateRenderComponent("rectangle"));

			if (!render->Initialise(sf::Vector2f(x, y), sf::Vector2f(width, height), sf::Color::Green))
			{
				Log::Message("Failed to create button, failed on render initialisation", Log::ERROR);
				return false;		//early out
			}
			render->SetOwner(button);
			button->AttachComponent(render);

			//script
			GUIButtonScript* script = new GUIButtonScript;
			script->SetOwner(button);

			if (!script->Initialise())
			{
				Log::Message("Failed to create button, failed on script initialisation", Log::ERROR);
				return false;
			}

			button->AttachComponent(script);

			pObject = *button;

			m_objectManager->AttachObject(button, GUIOBJECT);

			Log::Message("Button created", Log::SUCCESSFUL);

			return true;
		}

		bool GUIFactory::CreateCheckBox()
		{
			return true;
		}

	}
}