#include "stdafx.h"

#include "NavigationNode.h"

namespace wh
{

	NavigationNode::NavigationNode(sf::Vector2f pPosition, sf::Vector2i pGridIndex, bool pWalkable, float pTerrainToughness)
	{
		m_position = pPosition;
		m_gridIndex = pGridIndex;
		m_walkable = pWalkable;
		m_pathCount = 0;
		m_dirty = false;
		m_occupant = nullptr;
	}

	NavigationNode::~NavigationNode()
	{
		m_occupant = nullptr;
		m_parent = nullptr;
	}

	void NavigationNode::Occupy(Object* pObject, bool pWalkable)
	{
		m_occupant = pObject;
		m_walkable = pWalkable;
		m_dirty = true;
	}

	void NavigationNode::Leave()
	{
		m_occupant = nullptr;
	}

	Object* NavigationNode::getOccupant()
	{
		return m_occupant;
	}

	bool NavigationNode::isWalkable()
	{
		return m_walkable;
	}

	void NavigationNode::subscribe()
	{
		m_pathCount++;
	}

	void NavigationNode::unsubscribe()
	{
		m_pathCount--;
		if (m_pathCount <= 0)
			m_dirty = false;
	}

	bool NavigationNode::getDirty()
	{
		return m_dirty;
	}

	sf::Vector2f NavigationNode::getPosition()
	{
		return m_position;
	}

	sf::Vector2i NavigationNode::getGridIndex()
	{
		return m_gridIndex;
	}

	float NavigationNode::GetTerrainToughness()
	{
		return m_terrainToughness;
	}

}